#!/bin/bash

SELF="$(realpath "${BASH_SOURCE[0]}")"
DIR="$(cd "$(dirname "${SELF}")"; pwd -P)"

ln -s "${DIR}/bash_profile.sh" ~/.bash_profile
ln -s "${DIR}/bashrc.sh" ~/.bashrc
