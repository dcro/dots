# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


if [ -f /etc/profile ]; then
    source /etc/profile
fi

if [ -f ~/.bash_profile ]; then
    source ~/.bash_profile
fi
