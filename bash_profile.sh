SELF="$(realpath "${BASH_SOURCE[0]}")"
DIR="$(cd "$(dirname "${SELF}")"; pwd -P)"

echo "Processing bashrc" > "${DIR}/log"

for d in $(ls "${DIR}"/*/profile); do
	echo "Loading ${d}" >> "${DIR}/log"
	source "${d}" >> "${DIR}/log"
done
